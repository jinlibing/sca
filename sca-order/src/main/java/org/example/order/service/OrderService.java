package org.example.order.service;


import org.example.order.param.AddOrderParam;

public interface OrderService {

    String addOrder(AddOrderParam addOrderParam);
}


