package org.example.product.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.example.product.entity.Product;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductMapper extends BaseMapper<Product> {


}

