package org.example.product.service.impl;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.example.product.entity.Product;
import org.example.product.mapper.ProductMapper;
import org.example.product.service.ProductService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductMapper productMapper;

    @Override
    public Integer getUnitPrice(String productNo) {
        Product product = productMapper.selectOne(
                Wrappers.<Product>lambdaQuery()
                        .eq(Product::getProductNo, productNo)
                        .eq(Product::getStatus, 1));
        if (product == null || product.getUnitPrice() == null) {
            return -1;
        }

//        int a = 1/0;

        return product.getUnitPrice();
    }
}


