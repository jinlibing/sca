package org.example.stock.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.example.stock.entity.Stock;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StockMapper extends BaseMapper<Stock> {


}


