package org.example.es.controller;


import org.example.es.mapper.DocumentMapper;
import org.example.es.model.Document;
import lombok.RequiredArgsConstructor;
import org.dromara.easyes.core.conditions.select.LambdaEsQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
//Lombok官方给出的解释是: Generates constructor that takes one argument per final / non-null field.
// 所以它会为final和nonnull的属性作为参数产生一个构造函数。
public class EsController {

    private final DocumentMapper documentMapper;

    @GetMapping("/createIndex")
    public Boolean createIndex() {
        // 1.初始化-> 创建索引(相当于mysql中的表)
        return documentMapper.createIndex();
    }

    @GetMapping("/insert")
    public Integer insert() {
        // 2.初始化-> 新增数据
        Document document = new Document();
        document.setTitle("老汉");
        document.setContent("推2*技术过硬1");
        return documentMapper.insert(document);
    }

    @GetMapping("/search")
    public List<Document> search() {
        // 3.查询出所有标题为老汉的文档列表
        LambdaEsQueryWrapper<Document> wrapper = new LambdaEsQueryWrapper<>();
        wrapper.eq(Document::getTitle, "老汉");
        return documentMapper.selectList(wrapper);
    }

    @GetMapping("/like")
    public List<Document> like(@RequestParam(value = "q")String q) {
        // 3.查询出所有标题为老汉的文档列表
        LambdaEsQueryWrapper<Document> wrapper = new LambdaEsQueryWrapper<>();
        wrapper.like(Document::getContent, q);
        return documentMapper.selectList(wrapper);
    }

    @GetMapping("/updateById")
    public Integer updateById() {
        // 2.初始化-> 新增数据
        Document document = new Document();
        document.setTitle("老汉");
        document.setContent("推*技术过硬1");
        document.setId("2E2uM4wBNXdf8gK72YYB");
        return documentMapper.updateById(document);
    }

    @GetMapping("/deleteById")
    public Integer deleteById() {
        return documentMapper.deleteById("2E2uM4wBNXdf8gK72YYB");
    }
}
