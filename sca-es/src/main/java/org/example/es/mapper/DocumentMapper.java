package org.example.es.mapper;


import org.example.es.model.Document;
import org.dromara.easyes.core.core.BaseEsMapper;

/**
 * Mapper
 * <p>
 * Copyright © 2021 xpc1024 All Rights Reserved
 **/
public interface DocumentMapper extends BaseEsMapper<Document> {
}
